-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at http://mozilla.org/MPL/2.0/.
-----------------------------------------------------------------------------
-- |
-- Module      :  System.TimeoutManager
-- Copyright   :  (c) Toralf Wittner <tw@dtex.org>
-- License     :  see LICENSE
--
-- Maintainer  :  Toralf Wittner <tw@dtex.org>
-- Stability   :  experimental
-- Portability :  non-portable (GHC extensions)
--
-- A TimeoutManager tracks a set of actions which are executed on timeout.
-- This module is heavily inspired by Warp's
-- <http://hackage.haskell.org/package/warp-2.1.5.1 internal timeout manager>
--
-----------------------------------------------------------------------------
module System.TimeoutManager
    ( TimeoutManager
    , Action
    , Rounds (..)
    , create
    , destroy
    , add
    , addPaused
    , addRepeated
    , addTrivial
    , resume
    , reset
    , cancel
    , pause
    , setIO
    ) where

import Control.Applicative
import Control.Concurrent
import Control.Exception
import Control.Monad
import Data.IORef

-- | A 'TimeoutManager' cycles with a fixed frequency through the list
-- of actions to determine the ones that are due.
-- When 'add'ing a new action, or 'reset'ing an existing one, the number of
-- those cycles (aka \"rounds\") have to be given.
newtype Rounds = Rounds Int deriving (Eq, Show)

-- | A 'TimeoutManager' keeps track of a set of actions which are executed
-- after their individual timeout occured.
--
-- /Please note:/ On timeout actions are executed synchronously. It is
-- therefore imperative that the actions are quick to finish, otherwise the
-- processing of subsequent actions is delayed accordingly.
newtype TimeoutManager = TimeoutManager { elements :: IORef [Action] }

-- Interal action state.
data State
    = Wait   !Int      -- wait for the next n rounds before execution
    | Cycle  !Int !Int -- repeat action every n rounds
    | Paused           -- paused actions are skipped during timeout checking
    | Canceled         -- the action has been canceled and will be discared silently

-- | A value of type @Action@ holds the actual IO action plus some internal
-- state information.
data Action = Action
    { action :: IORef (IO ())
    , state  :: IORef State
    }

-- | Create a timeout manager which checks every given number of
-- micro-seconds if an action should be executed.
create :: Int -> IO TimeoutManager
create n = do
    e <- spawn [] $ \r -> do
        threadDelay n
        prev <- atomicModifyIORef' r $ \x -> ([], x)
        next <- prune prev id
        atomicModifyIORef' r $ \x -> (next x, ())
    return $ TimeoutManager e
  where
    prune []     front = return front
    prune (a:aa) front = do
        s <- atomicModifyIORef' (state a) $ \x -> (newState x, x)
        case s of
            Wait 0 -> do
                x <- readIORef (action a)
                ignore x
                prune aa front
            Cycle _ 0 -> do
                x <- readIORef (action a)
                ignore x
                prune aa (front . (a:))
            Canceled -> prune aa front
            _        -> prune aa (front . (a:))

    newState (Wait k)    = Wait (k - 1)
    newState (Cycle m 0) = Cycle m m
    newState (Cycle m k) = Cycle m (k - 1)
    newState s           = s

-- | Stops a 'TimeoutManager' and optionally executes all
-- remaining actions which haven't been paused or canceled.
destroy :: TimeoutManager -> Bool -> IO ()
destroy tm exec = mask_ $ term (elements tm) >>= when exec . mapM_ f
  where
    f e = readIORef (state e) >>= \s -> case s of
        Wait  _   -> readIORef (action e) >>= ignore
        Cycle _ _ -> readIORef (action e) >>= ignore
        _         -> return ()

-- | Add some action to be executed after the given number of 'round's.
add :: TimeoutManager -> Rounds -> IO () -> IO Action
add tm (Rounds n) a = do
    r <- Action <$> newIORef a <*> newIORef (Wait n)
    atomicModifyIORef' (elements tm) $ \rr -> (r:rr, ())
    return r

-- | Add some action to be executed every given number of 'round's.
addRepeated :: TimeoutManager -> Rounds -> IO () -> IO Action
addRepeated tm (Rounds n) a = do
    r <- Action <$> newIORef a <*> newIORef (Cycle n n)
    atomicModifyIORef' (elements tm) $ \rr -> (r:rr, ())
    return r

-- | Add some action in pause state. The manager will ignore the action
-- until 'reset' or 'resume'd.
addPaused :: TimeoutManager -> IO () -> IO Action
addPaused tm a = do
    r <- Action <$> newIORef a <*> newIORef Paused
    atomicModifyIORef' (elements tm) $ \rr -> (r:rr, ())
    return r

-- | Add a trivial action (@return ()@) via 'addPaused'.
addTrivial :: TimeoutManager -> IO Action
addTrivial tm = addPaused tm (return ())

-- | Reset the given action, i.e. whatever its internal state it will be
-- re-scheduled to fire after the given number of rounds.
reset :: Action -> Rounds -> IO ()
reset a (Rounds n) = atomicModifyIORef' (state a) $ \s ->
    case s of
        Cycle _ _ -> (Cycle n n, ())
        _         -> (Wait n, ())

-- | Resume the given action, i.e. if currently paused, the action will be
-- re-scheduled to fire after the given number of rounds.
resume :: Action -> Rounds -> Bool -> IO ()
resume a (Rounds n) rep = atomicModifyIORef' (state a) $ \s ->
    case s of
        Paused -> (if rep then Cycle n n else Wait n, ())
        _      -> (s, ())

-- | Pauses this action, i.e. it will not fire and it's internal timeout
-- will be forgotten.
--
-- /Please note:/ If the action is not 'resume'd, 'reset' or 'cancel'd
-- it will linger around until the manager is destroyed.
pause :: Action -> IO ()
pause a = atomicWriteIORef (state a) Paused

-- | Cancels this action which will not be executed by this manager.
cancel :: Action -> IO ()
cancel a = atomicWriteIORef (state a) Canceled

-- | Replace the actual IO operation associated with this action.
setIO :: Action -> IO () -> IO ()
setIO a = atomicWriteIORef (action a)

-----------------------------------------------------------------------------
-- Internal

spawn :: a -> (IORef a -> IO ()) -> IO (IORef a)
spawn a f = do
    r <- newIORef a
    _ <- forkIO $ forever (mask_ (f r)) `catch` finish
    return r
  where
    finish :: AsyncException -> IO ()
    finish = const $ return ()

term :: IORef a -> IO a
term r = atomicModifyIORef r $ \x -> (throw ThreadKilled, x)

ignore :: IO () -> IO ()
ignore a = catch a (const $ return () :: SomeException -> IO ())

